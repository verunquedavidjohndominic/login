<?php 

session_start();

	include("connection.php");
	include("functions.php");


	if($_SERVER['REQUEST_METHOD'] == "POST")
	{
		//something was posted
		$user_name = $_POST['user_name'];
		$password = $_POST['password'];

		if(!empty($user_name) && !empty($password) && !is_numeric($user_name))
		{

			//read from database
			$query = "select * from users where user_name = '$user_name' limit 1";
			$result = mysqli_query($con, $query);

			if($result)
			{
				if($result && mysqli_num_rows($result) > 0)
				{

					$user_data = mysqli_fetch_assoc($result);
					
					if($user_data['password'] === $password)
					{

						$_SESSION['user_id'] = $user_data['user_id'];
						header("Location: index.php");
						die;
					}
				}
			}
			
			echo "wrong username or password!";
		}
		else
		{
			echo "wrong username or password!";
		}
	}

?>





<!DOCTYPE html>
<html>
<head>
	<title>Login</title>

	<!-- bootstrap dependency -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

	<!-- external CSS -->
	<link rel ="stylesheet" href="./css/style.css">

</head>
<body>

	  <!-- log in form -->
	  <section class="pt-5">
			<div class="container col-10 col-md-7 col-lg-5 p-3">
			<div class="row justify-content-center my-3 flex-column align-items-center rounded-corner" id="color-c5c6c7">

				<div class="col-md-10 my-3 text-center  px-5 pt-3 pb-5 bg-custom" id="contact">
					<h3 class="text-uppercase text-white">Log In</h3>
					
					<form method = "post">
						<div class="form-group">
						<label for="FormControlInput1">User Name</label>
						<input type="text" class="form-control rounded-pill" name="user_name" id="FormControlInput">
						</div>

						<div class="form-group">
						<label for="FormControlInput2">Password</label>
						<input type="password" class="form-control rounded-pill mb-4" name="password" id="FormControlInput">

						
						<input class="btn btn-block text-white FormControlBtn col-5 m-auto mb-5 rounded-pill p-3" 
						type="submit" id="button" type="submit" value="Login">
						
						<div class="mt-3">
							<a href="signup.php" class="text-white">Click to Signup</a>
						</div>	
						 
					</form>  
				</div>  
				</div>
			</div>
		</section>












































		<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>
</html>