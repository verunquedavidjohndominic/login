<?php 
session_start();

	include("connection.php");
	include("functions.php");


	if($_SERVER['REQUEST_METHOD'] == "POST")
	{
		
		$user_name = $_POST['user_name'];
		$password = $_POST['password'];

		if(!empty($user_name) && !empty($password) && !is_numeric($user_name))
		{

			//save to database
			$user_id = random_num(20);
			$query = "insert into users (user_id,user_name,password) values ('$user_id','$user_name','$password')";

			mysqli_query($con, $query);

			header("Location: login.php");
			die;
		}else
		{
			echo "Please enter some valid information!";
		}
	}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Signup</title>

	<!-- bootstrap dependency -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

	<!-- external CSS -->
	<link rel ="stylesheet" href="./css/style.css">


</head>
<body>




	<section class="pt-5">
			<div class="container col-10 col-md-7 col-lg-5 p-3">
			<div class="row justify-content-center my-3 flex-column align-items-center rounded-corner" id="color-c5c6c7">

				<div class="col-md-10 my-3 text-center  px-5 pt-3 pb-5 bg-custom" id="contact">
					<h3 class="text-uppercase text-white">Sign up</h3>
					
					<form >
						<div class="form-group">
						<label for="FormControlInput1">User Name</label>
						<input type="text" class="form-control rounded-pill" name="user_name" id="FormControlInput">
						</div>

						<div class="form-group">
						<label for="FormControlInput2">Password</label>
						<input type="password" class="form-control rounded-pill mb-4" name="password" id="FormControlInput">

						
						<input class="btn btn-block text-white FormControlBtn col-5 m-auto mb-5 rounded-pill p-3" 
						type="submit" id="button" type="submit" value="Sign Up">
						
						<div class="mt-3">
							<a href="login.php" class="text-white">Click to Login</a>
						</div>	
						 
					</form>  
				</div>  
				</div>
			</div>
		</section>













































		<!-- JS dependency -->
		<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>
</html>